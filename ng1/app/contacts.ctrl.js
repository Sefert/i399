(function () {
    'use strict';

    angular.module('app').controller('ContactsCtrl', Ctrl);

    function Ctrl($http, modalService, $location) {
        var vm = this;

        this.items = [];
        this.removeItem = removeItem;

        init();

        vm.add=add;

        function init() {
            $http.get('api/contacts').then(function (result) {
                vm.items = result.data;
                done:false;
            });
        }

        function  removeItem(id) {
            modalService.confirm()
                .then(function () {
                    return $http.delete('api/contacts/' + id);
                }).then(init);
        }

        function add() {
            $location.path('/new');
        }
    }
})();