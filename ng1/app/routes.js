(function () {

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {

        $routeProvider.when('/search', {
            templateUrl : 'app/contacts.html',
            controller : 'ContactsCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/addcontact.html',
            controller : 'AddContact',
            controllerAs : 'vm'
        }).when('/search/:id', {
            templateUrl : 'app/editcontact.html',
            controller : 'EditContact',
            controllerAs : 'vm'
        }).otherwise('/search');
    }
})();