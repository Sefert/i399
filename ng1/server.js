'use strict'

const express = require('express');
//const bodyParser = require('body-parser'); no more need of this after express 4.x. More at row 15.
const Dao = require('./dao.js'); //for future Node.js support reference: import Dao from "./dao.js";
//to look inside objects: var util = require('util'); console.log(util.inspect(Object));

const app = express();
const dao = new Dao();

app.use(express.static(__dirname+'/app/'));
app.use(express.static(__dirname));

//instead of: app.use(bodyParser.json());
//express 4.x  built in bodyParser
app.use(express.json());

app.get('/api/contacts', findContacts);
app.post('/api/contacts', insertContact);
app.put('/api/contacts/:id', updateContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', deleteMany);

dao.conn().then(() => {
    app.listen(3000, () => console.log('Server running on port 3000!'));})
    .catch((err) => {
    console.error('DB connection failed! : ' + err);
});

app.get('/hello', (req, res) => res.send('Hello World!'));

function findContacts(request, response){
    dao.findAll()
        .then((contacts) => {
            response.set('Content-Type', 'application/json');
            response.end(JSON.stringify(contacts));})
        .catch((err) => {
        console.error('Error: ' + err);
    });
}

function insertContact(request, response){
    dao.insertCon(request.body)
        .then(() => {
            response.end();})
        .catch((err) => {
        console.error('Error: ' + err);
    });
}

function updateContact(request, response){
    let contact = {
        name: request.body.name,
        phone: request.body.phone,
    };
    dao.editCon(request.params.id, contact)
        .then(() => {
            response.end();})
        .catch((err) => {
        console.error('Error: ' + err);
    });
}

function deleteContact(request, response){
    dao.deleteCon(request.params.id)
        .then(() => {
            response.end();})
        .catch((err) => {
        console.error('Error: ' + err);
    });
}

function deleteMany(request, response){
    dao.deleteMany(request.body)
        .then(() => {
            response.end();})
        .catch((err) => {
        console.error('Error: ' + err);
    });
}