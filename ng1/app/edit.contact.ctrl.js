(function () {
    'use strict';
    angular.module('app').controller('EditContact', Ctrl);

    function Ctrl($http, $routeParams, $location) {
        var vm = this;
        vm.item = {};

        vm.back=back;
        vm.save=save;

        $http.get('api/contacts/' + $routeParams.id).then(function (result) {
            vm.item = result.data;
        })
        function back() {
            $location.path('/search');
        }
        function save(item) {
            console.log(item);
            $http.put('api/contacts/' + item._id, item);
            $location.path('/search');
        }
    }

})();