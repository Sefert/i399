'use strict'

const mongoDb = require('mongodb');
const mongo = mongoDb.MongoClient;
const ObjectID = mongoDb.ObjectID;
const MONGODB_URI = 'mongodb://user2:123456@ds247439.mlab.com:47439/i399';
const COLLECTION = 'contacts';

class Dao {

    conn() {
        return mongo.connect(MONGODB_URI)
            .then((db) => {this.coll = db.collection(COLLECTION)});
    }

    findAll() {
        return this.coll.find().toArray();
    }
    insertCon(contact){
        return this.coll.insertOne(contact);
    }
    editCon(id, contact){
        id = new ObjectID(id); //lihtsalt string ei sobi
        //another way to change the contact id: contact._id = id;
        return this.coll.updateOne({_id: id}, contact);
    }
    deleteCon(id){
        return this.coll.deleteOne({_id: new ObjectID(id)});
    }
    deleteMany(idCollection){
        //https://stackoverflow.com/questions/18566590/remove-multiple-documents-from-mongo-in-a-single-query
        for (let i = 0; i < idCollection.length; i++)
            idCollection[i] = ObjectID(idCollection[i]);
        return this.coll.deleteMany({'_id':{'$in':idCollection}});
    }
}

module.exports = Dao;

// works aswell
// function Dao(conn){
//         this.conn = conn;
// }
// Dao.prototype.findAll = function findAll() {
//     return this.conn.find().toArray();
// }
