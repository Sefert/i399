(function () {
    'use strict';
    angular.module('app').controller('AddContact', Ctrl);

    function Ctrl($http, $location) {

        var vm = this;
        vm.newName = '';
        vm.newPhone = '';

        vm.addNew= addNew;
        vm.back=back;

//        this.addNew = addNew;

        function addNew() {
            var  newItem = {
                name: vm.newName,
                phone: vm.newPhone
            };

            $http.post('api/contacts', newItem);
            // vm.newName='';
            // vm.newPhone='';
            $location.path('/search');
        }

        function back() {
            $location.path('/search');
        }

    }
})();